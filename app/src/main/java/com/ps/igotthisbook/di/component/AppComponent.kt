package com.ps.igotthisbook.di.component

import android.app.Application
import com.ps.igotthisbook.di.modules.RemoteDataModule
import com.ps.igotthisbook.di.modules.RepositoryModule
import com.ps.igotthisbook.di.scpoes.ApplicationScope
import com.ps.igotthisbook.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton

@ApplicationScope
@Component(
    modules = [
        RemoteDataModule::class,
        RepositoryModule::class
    ]
)

interface AppComponent {
    fun inject(application: Application)
    fun inject(mainViewModel: MainViewModel)

}
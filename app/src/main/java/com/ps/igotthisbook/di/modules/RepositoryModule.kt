package com.ps.igotthisbook.di.modules

import com.ps.igotthisbook.di.scpoes.ApplicationScope
import com.ps.igotthisbook.remote.Api
import com.ps.igotthisbook.repository.BasicRepository
import com.ps.igotthisbook.repository.BasicRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    @ApplicationScope
    fun provideBasicRepository(remoteSource: Api): BasicRepository = BasicRepositoryImpl(remoteSource)
}
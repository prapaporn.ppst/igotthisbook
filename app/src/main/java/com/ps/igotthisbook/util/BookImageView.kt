package com.ps.igotthisbook.util

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class BookImageView : AppCompatImageView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        //        int height = (int) (width / 1.5);

        val newHeightMeasure = MeasureSpec.makeMeasureSpec(
            (width * 1.5).toInt(),
            MeasureSpec.EXACTLY
        )
        // Child Views
        super.onMeasure(widthMeasureSpec, newHeightMeasure)
        // Self
        setMeasuredDimension(width, newHeightMeasure)
    }

}
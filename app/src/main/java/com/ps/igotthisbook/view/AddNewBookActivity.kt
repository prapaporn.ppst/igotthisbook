package com.ps.igotthisbook.view


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.zxing.integration.android.IntentIntegrator
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.ps.igotthisbook.R
import com.ps.igotthisbook.databinding.ActivityAddNewBookBinding
import com.ps.igotthisbook.model.Book
import kotlinx.android.synthetic.main.activity_add_new_book.*
import java.io.File
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask

class AddNewBookActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddNewBookBinding
    private lateinit var mStorageRef: StorageReference
    private lateinit var mDatabaseRef: DatabaseReference
    private var mUploadTask: StorageTask<UploadTask.TaskSnapshot>? = null
    private var imgPath = ""
    private var isDelete = false
    private var book: Book? = null

    private var mode = 0

    companion object {
        val MODE_ADD = 888
        val MODE_VIEW = 999
        val MODE_EDIT = 777
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_new_book)

        initial()
        setupListener()
        setupModeView()
        hideSoftKeyboard()
    }

    private fun setupModeView() {
        when (mode) {
            MODE_ADD -> {
                binding.btnAdd.text = "Add"
                binding.btnScanCode.visibility = View.VISIBLE
                binding.btnAdd.visibility = View.VISIBLE
                binding.edtBookNumber.isEnabled = true
                binding.edtBookName.isEnabled = true
                binding.edtBookISBN.isEnabled = true
                binding.btnEdit.visibility = View.GONE
                binding.ivBook.isEnabled = true
                binding.btnDelete.visibility = View.GONE

            }
            MODE_VIEW -> {
                binding.btnScanCode.visibility = View.GONE
                binding.btnAdd.visibility = View.GONE
                binding.edtBookNumber.isEnabled = false
                binding.edtBookName.isEnabled = false
                binding.edtBookISBN.isEnabled = false
                binding.btnEdit.visibility = View.VISIBLE
                imgPath = book!!.imgPath
                binding.ivBook.isEnabled = false
                binding.btnDelete.visibility = View.VISIBLE


            }
            else -> {
                binding.btnAdd.text = "Save"
                binding.ivBook.isEnabled = true
                binding.btnScanCode.visibility = View.VISIBLE
                binding.btnAdd.visibility = View.VISIBLE
                binding.edtBookNumber.isEnabled = true
                binding.edtBookName.isEnabled = true
                binding.edtBookISBN.isEnabled = true
                binding.btnEdit.visibility = View.GONE
                binding.btnDelete.visibility = View.VISIBLE

            }
        }
    }

    private fun initial() {
        mode = intent.getIntExtra("mode", MODE_ADD)
        if (mode == MODE_VIEW) {
            book = intent.getBundleExtra("bundle")?.getParcelable("book")
            if (book != null) {
                showBookData()
            }
        }
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads")
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("books")
    }

    private fun setupListener() {
        binding.ivBook.setOnClickListener {
            checkPermission()
        }

        binding.btnAdd.setOnClickListener {
            if (mUploadTask != null) {
                if (mUploadTask!!.isInProgress) {
                    return@setOnClickListener
                }
            }

            edtBookName.error = null

            if (edtBookName.text.toString().trim().isNotEmpty()) {
                val aBook = Book(
                    System.currentTimeMillis().toString(),
                    edtBookName.text.toString().trim(),
                    edtBookNumber.text.toString().trim(),
                    imgPath,
                    edtBookISBN.text.toString().trim()
                )
                if (mode == MODE_ADD) {
                    val bId = mDatabaseRef.push().key
                    if (!bId.isNullOrEmpty())
                        mDatabaseRef.child(bId).setValue(aBook).addOnSuccessListener {
                            Toast.makeText(this@AddNewBookActivity, "Add new book successful.", Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        }.addOnFailureListener {
                            Toast.makeText(
                                this@AddNewBookActivity,
                                "Error occurred,Please try Again.",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                } else if (mode == MODE_EDIT) {
                    mDatabaseRef.orderByChild("bookId").equalTo(book!!.bookId)
                        .addListenerForSingleValueEvent(valueListener)

                }


            } else {
                edtBookName.error = "please enter book name."
                edtBookName.requestFocus()
            }
        }

        binding.btnScanCode.setOnClickListener {
            IntentIntegrator(this).initiateScan()
        }

        binding.btnEdit.setOnClickListener {
            mode = MODE_EDIT
            setupModeView()
        }

        binding.btnDelete.setOnClickListener {
            isDelete = true
            mDatabaseRef.orderByChild("bookId").equalTo(book!!.bookId)
                .addListenerForSingleValueEvent(valueListener)
        }

        binding.btnBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun checkPermission() {
        Dexter.withActivity(this)
            .withPermissions(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.INTERNET,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        if (report.areAllPermissionsGranted()) {
                            openChooserOption()
                        } else if (report.isAnyPermissionPermanentlyDenied) {
                            Toast.makeText(
                                this@AddNewBookActivity,
                                "Some permission denied,Please try again.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener { error ->
                Toast.makeText(this, "Error occurred! $error", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    private fun openChooserOption() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL)
            .single()
            .theme(R.style.AppTheme)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val img = ImagePicker.getFirstImageOrNull(data)
            if (img != null) {
                uploadFile(img)
            } else {
                Toast.makeText(this@AddNewBookActivity, "Error occurred,Please try Again.", Toast.LENGTH_SHORT).show()
            }
        }

        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Scanned", Toast.LENGTH_LONG).show()
                binding.edtBookISBN.setText(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getFileExtension(name: String): String {
        var fileExtension = ""
        val st = name.split(".")
        if (st.isNotEmpty()) {
            fileExtension = st.last()
        } else {
            Toast.makeText(this@AddNewBookActivity, "Error occurred,Please try Again.", Toast.LENGTH_SHORT).show()
        }
        return fileExtension
    }

    private fun updateImageView(path: Uri) {

        Glide.with(this)
            .load(path)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.progressBar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    binding.ivBook.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    return false
                }

            })
            .into(binding.ivBook)

    }

    private fun uploadFile(img: Image) {
        val fileExtension = getFileExtension(img.name)
        if (fileExtension.isNotEmpty()) {
            val fileRef = mStorageRef.child("${System.currentTimeMillis()}.$fileExtension")
            mUploadTask = fileRef.putFile(Uri.fromFile(File(img.path)))
                .addOnSuccessListener { taskSnapshot ->
                    Toast.makeText(this@AddNewBookActivity, "Upload successful.", Toast.LENGTH_SHORT).show()
                    fileRef.downloadUrl.addOnSuccessListener {
                        imgPath = it.toString()
                        updateImageView(it)

                    }

                }.addOnFailureListener { exception ->
                    Toast.makeText(this@AddNewBookActivity, exception.message, Toast.LENGTH_SHORT)
                        .show()
                }.addOnProgressListener { taskSnapshot ->
                    binding.ivBook.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                }

        } else {
            Toast.makeText(this@AddNewBookActivity, "Error occurred,Please try Again.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showBookData() {
        binding.edtBookISBN.setText(book!!.isbncode)
        binding.edtBookName.setText(book!!.name)
        binding.edtBookNumber.setText(book!!.bookNumber)
        Glide.with(this).load(book!!.imgPath).into(binding.ivBook)
    }

    private val valueListener = object : ValueEventListener {
        override fun onCancelled(error: DatabaseError) {
            Toast.makeText(this@AddNewBookActivity, error.message, Toast.LENGTH_SHORT)
                .show()
        }

        override fun onDataChange(snapshot: DataSnapshot) {
            var key: String? = null
            if (snapshot.exists()) {
                for (item in snapshot.children) {
                    key = item.key
                }
                if (key != null) {
                    if (!isDelete)
                        updateBookData(key)
                    else
                        deleteBook(key)
                } else {
                    Toast.makeText(
                        this@AddNewBookActivity,
                        "Error occurred,Please try Again.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    this@AddNewBookActivity,
                    "Error occurred,Please try Again.",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }


    }

    private fun deleteBook(key: String) {
        mDatabaseRef.child(key).removeValue().addOnCanceledListener {
            Toast.makeText(this@AddNewBookActivity, "Error occurred,Please try Again.", Toast.LENGTH_SHORT).show()
            isDelete = false

        }.addOnSuccessListener {
            isDelete = false
            Toast.makeText(this@AddNewBookActivity, "Deleted.", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun updateBookData(key: String) {
        val nBook =
            Book(
                book!!.bookId,
                edtBookName.text.toString(),
                edtBookNumber.text.toString(),
                imgPath,
                edtBookISBN.text.toString()
            )
        mDatabaseRef.child(key).setValue(nBook).addOnSuccessListener {
            Toast.makeText(this@AddNewBookActivity, "Add new book successful.", Toast.LENGTH_SHORT)
                .show()
            book = nBook
            showBookData()
            mode = MODE_VIEW
            setupModeView()
            //finish()
        }.addOnFailureListener {
            Toast.makeText(
                this@AddNewBookActivity,
                "Error occurred,Please try Again.",
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun hideSoftKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    override fun onBackPressed() {
        val name = edtBookName.text.toString().trim()
        val isbn = edtBookISBN.text.toString().trim()
        val num = edtBookNumber.text.toString().trim()
        if (MODE_VIEW == mode) {
            super.onBackPressed()
        } else {
            if (imgPath.isEmpty() && name.equals("") && isbn.equals("") && num.equals("")) {
                super.onBackPressed()
            } else {
                showWarningDialog()
            }
        }

    }

    private fun showWarningDialog() {
        val builder = AlertDialog.Builder(this)

        builder.setTitle("Confirm Exit")
        builder.setMessage("Exit without saving?")

        builder.setPositiveButton("YES") { dialog, which ->
            finish()
        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.dismiss()
        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}

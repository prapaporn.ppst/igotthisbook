package com.ps.igotthisbook.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.database.*
import com.ps.igotthisbook.MainApplication
import com.ps.igotthisbook.R
import com.ps.igotthisbook.adapter.BookAdapter
import com.ps.igotthisbook.callback.BookCallback
import com.ps.igotthisbook.databinding.ActivityMainBinding
import com.ps.igotthisbook.model.Book
import com.ps.igotthisbook.util.SpacesItemDecoration
import com.ps.igotthisbook.viewmodel.MainViewModel

class MainActivity : AppCompatActivity(), BookCallback {


    lateinit var binding: ActivityMainBinding
    lateinit var adapter: BookAdapter

    lateinit var mDatabaseRef: DatabaseReference
    private var bookList = mutableListOf<Book>()

    //TODO : check connection


    val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java).also {
            MainApplication.appComponent.inject(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        hideContent()
        setupRecyclerView()
        initial()
    }

    override fun onResume() {
        super.onResume()
        getAllBook()
    }

    private val valueListener = object : ValueEventListener {
        override fun onCancelled(error: DatabaseError) {
            Toast.makeText(this@MainActivity, error.message, Toast.LENGTH_SHORT)
                .show()
        }

        override fun onDataChange(snapshot: DataSnapshot) {
            bookList.clear()
            if (snapshot.exists()) {
                for (item in snapshot.children) {
                    val book = item.getValue(Book::class.java)
                    if (book != null)
                        bookList.add(book)
                }
            }
            adapter.updateBookData(bookList)
            viewModel.originalBookList = bookList
            showContent()
        }

    }

    private fun showErrorView() {
        Toast.makeText(this, "Internrt error", Toast.LENGTH_SHORT).show()
    }


    private fun showContent() {
        binding.rvBook.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
    }

    private fun hideContent() {
        binding.rvBook.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun getAllBook() {
        hideEmptyView()
        adapter.setupIsSearch(false)
        mDatabaseRef.addListenerForSingleValueEvent(valueListener)
    }


//    private val obs = Observer<List<Book>> {
//        onGetBookDataResult(it)
//    }

//    private fun onGetBookDataResult(book: List<Book>?) {
//        if (!book.isNullOrEmpty()) {
//            adapter.updateBookData(book)
//        } else {
//
//        }
//    }

    private fun searchBook(searchText: String) {
        val result = viewModel.searchBook(searchText)
        adapter.setupIsSearch(true)
        if (result.isNotEmpty()) {
            adapter.updateBookData(result)
            hideEmptyView()
        } else {
            showEmptyView()
        }
    }

    private fun showEmptyView() {
        binding.emptyView.rlEmptyView.visibility = View.VISIBLE
    }

    private fun hideEmptyView() {
        binding.emptyView.rlEmptyView.visibility = View.GONE
    }

    private fun initial() {
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("books")

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (binding.searchView.query.isEmpty())
                    getAllBook()
                else
                    searchBook(binding.searchView.query.toString())
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.searchView.query.isEmpty())
                    getAllBook()
                return false
            }

        })
        //viewModel.getBookData()?.observe(this, obs)
    }

    private fun setupRecyclerView() {
        adapter = BookAdapter(this)
        binding.rvBook.layoutManager = GridLayoutManager(this, 3)
        binding.rvBook.addItemDecoration(SpacesItemDecoration(8))
        binding.rvBook.adapter = adapter
    }

    private fun openAddNewBook() {
        val intent = Intent(this, AddNewBookActivity::class.java)
        intent.putExtra("mode", AddNewBookActivity.MODE_ADD)
        startActivity(intent)
    }

    override fun onBookClick(book: Book) {
        val intent = Intent(this, AddNewBookActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable("book", book)
        intent.putExtra("bundle", bundle)
        intent.putExtra("mode", AddNewBookActivity.MODE_VIEW)
        startActivity(intent)
    }

    override fun onAddClick() {
        openAddNewBook()
    }
}

package com.ps.igotthisbook

import android.app.Application
import com.ps.igotthisbook.di.component.AppComponent
import com.ps.igotthisbook.di.component.DaggerAppComponent
import com.ps.igotthisbook.di.modules.RemoteDataModule
import com.ps.igotthisbook.di.modules.RepositoryModule

class MainApplication : Application() {

    companion object {
        //platformStatic allow access it from java code
        @JvmStatic
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        val useUrl = "https://mytestproject-e23cd.firebaseio.com"

        appComponent = DaggerAppComponent.builder()
            .remoteDataModule(RemoteDataModule(useUrl))
            .repositoryModule(RepositoryModule())
            .build()

        appComponent.inject(this)

    }
}
package com.ps.igotthisbook.callback

import com.ps.igotthisbook.model.Book

interface BookCallback {
    fun onBookClick(book: Book)
    fun onAddClick()
}
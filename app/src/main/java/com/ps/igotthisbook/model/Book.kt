package com.ps.igotthisbook.model

import android.os.Parcel
import android.os.Parcelable

class Book(
    val bookId: String = "",
    val name: String = "",
    val bookNumber: String = "",
    val imgPath: String = "",
    val isbncode: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bookId)
        parcel.writeString(name)
        parcel.writeString(bookNumber)
        parcel.writeString(imgPath)
        parcel.writeString(isbncode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Book> {
        override fun createFromParcel(parcel: Parcel): Book {
            return Book(parcel)
        }

        override fun newArray(size: Int): Array<Book?> {
            return arrayOfNulls(size)
        }
    }

}
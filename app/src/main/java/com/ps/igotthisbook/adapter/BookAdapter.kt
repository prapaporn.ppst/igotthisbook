package com.ps.igotthisbook.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ps.igotthisbook.callback.BookCallback
import com.ps.igotthisbook.databinding.ViewholderAddNewItemBinding
import com.ps.igotthisbook.databinding.ViewholderBookItemBinding
import com.ps.igotthisbook.model.Book

class BookAdapter(val callback: BookCallback) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var bookData = mutableListOf<Book>()
    private val TYPE_ADD = 111
    private val TYPE_VIEW = 222
    lateinit var context: Context
    private var isSearch = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)

        if (viewType == TYPE_VIEW) {
            val vh: ViewholderBookItemBinding =
                ViewholderBookItemBinding.inflate(layoutInflater, parent, false)
            return BookItemHolder(vh)
        } else {
            val vh: ViewholderAddNewItemBinding =
                ViewholderAddNewItemBinding.inflate(layoutInflater, parent, false)
            return AddNewItemHolder(vh)
        }
    }

    override fun getItemCount(): Int {
        if (isSearch)
            return bookData.size
        return bookData.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BookItemHolder) {
            Glide.with(context).load(bookData[position].imgPath).into(holder.binding.ivBook)
            holder.binding.tvBookName.text = bookData[position].name
            holder.binding.item.setOnClickListener {
                callback.onBookClick(bookData[position])
            }

        }

        if (holder is AddNewItemHolder) {
            holder.binding.rlAddNewBook.setOnClickListener {
                callback.onAddClick()
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isSearch) {
            TYPE_VIEW
        } else {
            if (position == itemCount - 1) {
                TYPE_ADD
            } else {
                TYPE_VIEW
            }
        }
    }

    fun updateBookData(book: List<Book>) {
        bookData = book.toMutableList()
        notifyDataSetChanged()
    }

    fun setupIsSearch(isSearch: Boolean) {
        this.isSearch = isSearch
    }

    inner class BookItemHolder(val binding: ViewholderBookItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            binding.executePendingBindings()
        }

    }

    inner class AddNewItemHolder(val binding: ViewholderAddNewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            binding.executePendingBindings()
        }
    }
}
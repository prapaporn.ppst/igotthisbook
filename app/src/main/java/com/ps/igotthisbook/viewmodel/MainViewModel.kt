package com.ps.igotthisbook.viewmodel

import androidx.lifecycle.ViewModel
import com.ps.igotthisbook.model.Book
import com.ps.igotthisbook.repository.BasicRepository
import javax.inject.Inject

class MainViewModel : ViewModel() {


    @Inject
    lateinit var repo: BasicRepository

    var originalBookList = mutableListOf<Book>()
    //var searchList = mutableListOf<Book>()


    fun searchBook(searchText: String): List<Book> {
        return originalBookList.filter { it.name.toLowerCase().contains(searchText.toLowerCase()) }
    }
}